import java.io.File;
import java.util.Scanner;
import java.util.Arrays;

public class Graph {
	int numVertex; // Number of vertices in the graph.
	GraphNode[] G; // Adjacency list for graph.
	String graphName; // The file from which the graph was created.

	public Graph() {
		this.numVertex = 0;
		this.graphName = "";
	}

	public Graph(int numVertex) {
		this.numVertex = numVertex;
		G = new GraphNode[numVertex];
		for (int i = 0; i < numVertex; i++) {
			G[i] = new GraphNode(i);
		}
	}

	public boolean addEdge(int source, int destination) {
		if (source < 0 || source >= numVertex)
			return false;
		if (destination < 0 || destination >= numVertex)
			return false;
		// add edge
		G[source].addEdge(source, destination);
		return true;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("The Graph " + graphName + " \n");

		for (int i = 0; i < numVertex; i++) {
			sb.append(G[i].toString());
		}
		return sb.toString();
	}

	public void makeGraph(String filename) {
		try {
			graphName = filename;
			Scanner reader = new Scanner(new File(filename));
			System.out.println("\n" + filename);
			numVertex = reader.nextInt();
			G = new GraphNode[numVertex];
			for (int i = 0; i < numVertex; i++) {
				G[i] = new GraphNode(i);
			}
			while (reader.hasNextInt()) {
				int v1 = reader.nextInt();
				int v2 = reader.nextInt();
				G[v1].addEdge(v1, v2);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearAllPred() {
		for (int i = 0; i < numVertex; i++) {
			G[i].p1.clear();
			G[i].p2.clear();
		}
	}

	/**
	 * Computes the least common ancestor of v1 and v2, prints the length of the
	 * path, the ancestor, and the path itself.
	 *
	 * @param v1: first vertex
	 * @param v2: second vertex
	 * @return returns the length of the shortest ancestral path.
	 */
	public void lca(int v1, int v2) {
		// Computes the path info for both vertices
		PathInfo[] v1PathInfo = fillPathInfo(v1);
		PathInfo[] v2PathInfo = fillPathInfo(v2);
		// By setting it to something less than 0, its impossible to get that and makes
		// it the perfect for the checker
		int minimumDistance = -1;
		int minimumVertex = -1;
		// Adds the distance
		for (int i = 0; i < G.length; i++) {
			// Only checks the path info if they're not null, because we made a promise
			if (v1PathInfo[i] != null && v2PathInfo[i] != null) {
				int sumDistance = v1PathInfo[i].dist + v2PathInfo[i].dist;
				if (sumDistance < minimumDistance || minimumDistance < 0) {
					minimumDistance = sumDistance;
					minimumVertex = i;
				}
			}
		}
		int[] minimumPath = new int[minimumDistance + 1];
		PathInfo path1 = v1PathInfo[minimumVertex];
		int firstVertex = minimumVertex;
		// If less than 0, then there is no connection
		if (minimumDistance < 0) {
			System.out.println("There is no common ancestor between " + v1 + " and " + v2);
		}
		// The nodes are the same
		else if (minimumDistance == 0) {
			System.out.println(graphName + " Best lca " + v1 + " " + v2 + " Distance: " + 0 + " Ancestor "
					+ minimumVertex + " Path:" + v1);
			// Returns an actual distance
		} else {

			// This adds the nodes from the first node to where the second one meets up
			for (int i = path1.dist; i >= 0; i--) {
				minimumPath[i] = firstVertex;
				path1 = v1PathInfo[firstVertex];
				firstVertex = path1.pred;
			}
			// This adds the second nodes from where it meets up with the first one
			path1 = v2PathInfo[minimumVertex];
			int vertex2 = minimumVertex;
			for (int i = path1.dist; i >= 0; i--) {
				minimumPath[minimumDistance - i] = vertex2;
				path1 = v2PathInfo[vertex2];
				vertex2 = path1.pred;
			}
			System.out.print(graphName + " Best Least Common Ancestor " + v1 + " " + v2 + " Distance: "
					+ minimumDistance + " Ancestor " + minimumVertex + " Path:");
			for (int i = 0; i < minimumPath.length; i++) {
				System.out.print(minimumPath[i] + " ");
			}
			System.out.println();
		}
	}

	// This fills the path info of a specific node
	private PathInfo[] fillPathInfo(int v1) {
		// Sets it to length of G
		PathInfo[] pathInfos = new PathInfo[G.length];
		pathInfos[v1] = new PathInfo();
		pathInfos[v1].dist = 0;
		pathInfos[v1].pred = v1;
		// Fills the paths
		fillPaths(v1, 1, pathInfos);
		return pathInfos;
	}

	private void fillPaths(int index, int currentDist, PathInfo[] pathInfo) {
		GraphNode populateGraphNode = G[index];

		for (EdgeInfo edgeInfo : populateGraphNode.succ) {
			int succ = edgeInfo.to;
			if (pathInfo[succ] == null || pathInfo[succ].dist > currentDist) {
				pathInfo[succ] = new PathInfo();
				pathInfo[succ].dist = currentDist;
				pathInfo[succ].pred = index;
			}
			fillPaths(succ, currentDist + 1, pathInfo); // makes another call for each successor.
		}
	}

	/**
	 * Public method to find the outcast of a set of nodes. The outcast is the
	 * "least connected" node. Connectedness calculated for each node using its sum
	 * of its path lengths.
	 * 
	 * @param v the array or set of nodes being looked at.
	 *
	 * @return the outcast value
	 */
	public void outcast(int[] v) {
		int outcast = 0;
		int greatestIndex = 0;
		for (int i = 0; i < v.length; i++) {
			PathInfo[] infos = fillPathInfo(v[i]);
			int currentSum = 0;
			for (int j = 0; j < infos.length; j++) {
				if (infos[j] != null) {
					currentSum += infos[j].dist;
				}
			}
			if (currentSum > outcast) {
				outcast = currentSum;
				greatestIndex = v[i];
			}
		}
		System.out.println(
				"The outcast of " + Arrays.toString(v) + " is " + greatestIndex + " with distance sum of " + outcast);
	}

	public static void main(String[] args) {
		Graph graph1 = new Graph();
		graph1.makeGraph("digraph1.txt");
		// System.out.println(graph.toString());
		int[] set1 = { 7, 10, 2 };
		int[] set2 = { 7, 17, 5, 11, 4, 23 };
		int[] set3 = { 10, 17, 13 };

		graph1.lca(3, 7);
		graph1.lca(5, 6);
		graph1.lca(9, 1);
		graph1.outcast(set1);

		Graph graph2 = new Graph();
		graph2.makeGraph("digraph2.txt");
		// System.out.println(graph2.toString());
		graph2.lca(3, 24);

		graph2.outcast(set3);
		graph2.outcast(set2);
		graph2.outcast(set1);
	}
}